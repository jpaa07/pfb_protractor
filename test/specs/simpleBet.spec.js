import loginPage from "../../src/pages/login.page"
import homePage from "../../src/pages/home.page"
import eventsPage from "../../src/pages/events.page"
import betPage from "../../src/pages/bet.page"
import loginData from "../resources/data/login_data"

describe("Make a simple bet",() => {

    beforeEach(() =>{
        browser.get(loginPage.url);
    });

    it("should be able to login into the app",() =>{
        loginPage.login(loginData.username,loginData.password);
        expect(homePage.isLoaded()).toBe(true);
    });

    it("should be able to select an aleathory event",() =>{
        homePage.getAleathoryEvent();
        expect(betPage.isLoaded()).toBe(true);
        betPage.makeABet();
        browser.sleep(5000);
    });
});