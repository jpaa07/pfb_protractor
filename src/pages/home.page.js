browser.ignoreSynchronization = true;

import MainPage from "./main.page";
import { element, ExpectedConditions } from "protractor";

class HomePage extends MainPage{

    constructor(){
        super();
        this.btnEvents = $('.gtm-events');
        this.eventsList = $$('.media__body > .elltext');
        this.pageLoaded = this.waitElementUntilPresent($('.user-list__title'));
    }

    navigateToEvents(){
         return this.btnEvents.click();
    }

    getAleathoryEvent(){
        let event;
        return this.eventsList.then((events) => {
           this.waitElementUntilClickable(events[0]);
           const numberEvents = events.length - 3;
           let aleathoryEvent = Math.floor(Math.random() * numberEvents) + 0;
           event = events[aleathoryEvent];
         }).then(()=>{
             this.waitElementUntilPresent(event);
             event.click();
         });
   }
}

export default new HomePage();