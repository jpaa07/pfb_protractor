browser.ignoreSynchronization = true;

import MainPage from "./main.page";
import { $$ } from "protractor";

class EventPage extends MainPage {

    constructor(){
        super()
        this.btnHighlightEvents = $('.gtm-event-nav-popular');
        this.pageLoaded = this.waitElementUntilPresent($('.gtm-event-nav-popular'));
    }

    navigateToHighLightEvents(){
        browser.sleep(5000);
        return this.btnHighlightEvents.click();
    }
}


export default new EventPage();