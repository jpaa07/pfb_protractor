import MainPage from "./main.page";

class BetPage extends MainPage {
    constructor(){
        super();
        this.betsPoints = $$('.bet-option > .right');
        this.pageLoaded = this.waitElementUntilPresent($('.im-event-options'));
        this.home;
        this.tie;
        this.visit;
    }

    async makeABet(){
        this.betsPoints.then((bets) =>{
            const options = bets.length;
            console.log("length: " + bets.length);
            if (options == 2){
                bets[0].getText().then((text)=>{
                    this.home = text;
                });
                bets[1].getText().then((text)=>{
                    this.visit = text;
                });
            } else {
                bets[0].getText().then(async (text)=>{
                    this.home = text;
                });
                bets[1].getText().then((text)=>{
                    this.tie = text;
                });
                bets[2].getText().then((text)=>{
                    this.visit = text;
                });
            }
        }).then(()=>{
            
        });
    }

    analizeBet(){

    }
}



export default new BetPage();