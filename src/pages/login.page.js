browser.ignoreSynchronization = true;
import MainPage from "./main.page";

class LoginPage extends MainPage {
    constructor(){
        super();
        this.url = "/";
        this.btnGotoLogin = $(".btn--submit");
        this.txtUsername = $("#user_login");
        this.txtPassword = $("#user_password");
        this.submitLogin = element(by.name("button"));
    }

    login(username,password){
        this.btnGotoLogin.click();
        this.txtUsername.sendKeys(username);
        this.txtPassword.sendKeys(password);
        return this.submitLogin.click();
    }
}

export default new LoginPage();