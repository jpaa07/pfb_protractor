import { browser } from "protractor";

export default class MainPage {
    constructor() {
        this.timeout = {
            'bitWait': 1000,
            'littleWait' : 3000,
            'mediumWait' : 5000,
            'largeWait' : 9000,
        };
    }

    isLoaded() {
        return browser.wait(() => {
            return this.pageLoaded();
        }, this.timeout.largeWait);
    }

    waitElementUntilVisible(locator) {
        return browser.wait(protractor.ExpectedConditions.visibilityOf(locator)
        , this.timeout.largeWait);
    }

    waitElementUntilPresent(locator) {
        return protractor.ExpectedConditions.presenceOf(locator);
    }

    waitElementUntilClickable(locator) {
        return browser.wait(protractor.ExpectedConditions.elementToBeClickable(locator),
        this.timeout.largeWait);    
    }

    enter() {
        return browser.actions().sendKeys(protractor.Key.ENTER).perform();
    }
}